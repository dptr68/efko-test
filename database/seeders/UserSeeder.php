<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'fields' => [
                    'name' => 'Test Lead',
                    'email' => 'lead@mail.com',
                    'password' => bcrypt('test'),
                ],
                'role' => Role::LEAD
            ],
            [
                'fields' => [
                    'name' => 'Test Employee 1',
                    'email' => 'employee1@mail.com',
                    'password' => bcrypt('test'),
                ],
                'role' => Role::EMPLOYEE
            ],
            [
                'fields' => [
                    'name' => 'Test Employee 2',
                    'email' => 'employee2@mail.com',
                    'password' => bcrypt('test'),
                ],
                'role' => Role::EMPLOYEE
            ]
        ];

        foreach ($users as $user) {
            if (User::query()->firstWhere('email', $user['fields']['email'])) {
                continue;
            }

            $createdUser = User::query()->create($user['fields']);
            $createdUser->roles()->attach($user['role']);
        }

    }
}
