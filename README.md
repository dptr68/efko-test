# copy .env
$ cp .env.example .env

# run docker
$ cd docker-laravel
$ docker-compose up --build -d
$ docker-compose exec php-fpm bash
# install dependencies
$ composer i
# generate key
$ php artisan key:generate
# run migrate
$ php artisan migrate
# run db seed
$ php artisan db:seed
  
#link site  
    http://localhost
  
# test users  
    lead
    email: lead@mail.com
    pass: test 
     
    employee1 
    email: employee1@mail.com
    pass: test
    
    employee2
    email: employee2@mail.com
    pass: test





