<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


# auth routes
Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return redirect('/vacations');
    })->name('dashboard');

    # vacations list routes
    Route::prefix('vacations')->as('vacations.')->group(function () {
        Route::get('/', [\App\Http\Controllers\VacationController::class, 'index'])->name('index');
        Route::get('/search', [\App\Http\Controllers\VacationController::class, 'search'])->name('search');
        Route::post('/', [\App\Http\Controllers\VacationController::class, 'save'])->name('save');
        Route::post('/{vacation}/apply', [\App\Http\Controllers\VacationController::class, 'apply'])->name('apply');
    });
});

require __DIR__.'/auth.php';
