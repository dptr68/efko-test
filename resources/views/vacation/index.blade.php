<x-app-layout>
    <x-slot name="header">

    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if(session()->exists('error_search'))
            <div class="col-span-12 text-red-800">
                {{ session()->get('error_search') }}
            </div>
            @endif
            <form action="{{ route('vacations.search') }}" method="GET">
                <div class="grid grid-cols-12 gap-10">
                    <div class="col-span-6">
                        <label for="date_from" class="block font-medium text-gray-700">Дата начала</label>
                        <input type="date" name="date_from" id="date_from" value="{{ request()->input('date_from') }}"
                               class="mt-1 focus:ring-blue-400 focus:border-blue-400 block w-full shadow-sm border-gray-300">
                    </div>
                    <div class="col-span-6">
                        <label for="date_to" class="block font-medium text-gray-700">Дата окончания</label>
                        <input type="date" name="date_to" id="date_to" value="{{ request()->input('date_to') }}"
                               class="mt-1 focus:ring-blue-400 focus:border-blue-400 block w-full shadow-sm border-gray-300">
                    </div>


                </div>
                <div class="grid grid-cols-12 gap-20 my-4">
                    <div class="col-span-1 self-end">
                        <button type="submit"
                                class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-semibold rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:none">
                            Найти
                        </button>
                    </div>
                    <div class="col-span-1 self-end">
                        <a href="{{ route('vacations.index') }}"
                           class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-semibold rounded-md text-white bg-gray-400 hover:bg-gray-500 focus:outline-none focus:none">
                            Сбросить
                        </a>
                    </div>
                </div>
            </form>

            @if((isset($isApplied) && !$isApplied) && request()->exists('date_from') && request()->exists('date_to') && auth()->user()->hasRole(\App\Models\Role::EMPLOYEE))
            <form action="{{ route('vacations.save') }}" method="post" class="mt-2">
                @csrf
                <input type="hidden" name="date_from" value="{{ request()->input('date_from') }}">
                <input type="hidden" name="date_to" value="{{ request()->input('date_to') }}">
                <button type="submit"
                        class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-semibold rounded-md text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:none">
                    Запланировать
                </button>
            </form>
            @endif

            <div class="flex flex-col mt-8">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="shadow overflow-hidden border-b border-gray-200">
                            <table class="min-w-full divide-y divide-gray-400">
                                <thead class="bg-gray-200">
                                <tr>
                                    <th scope="col"
                                        class="px-6 py-3 text-left text-sm text-black-500 tracking-wider">
                                        Сотрудник
                                    </th>
                                    <th scope="col"
                                        class="px-6 py-3 text-sm text-left text-black-500 tracking-wider">
                                        Дата начала
                                    </th>
                                    <th scope="col"
                                        class="px-6 py-3 text-left text-sm text-black-500 tracking-wider">
                                        Дата окончания
                                    </th>
                                    <th scope="col"
                                        class="px-6 py-3 text-left text-sm text-black-500 tracking-wider">
                                        Подтвержден
                                    </th>
                                    <th scope="col" class="relative px-6 py-3">

                                    </th>
                                </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                @forelse($vacations as $vacation)
                                <tr>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="flex items-center">
                                            <div>
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{ $vacation->user->name }}
                                                </div>
                                                <div class="text-sm text-gray-500">
                                                    {{ $vacation->user->email }}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">
                                            {{ $vacation->date_from->format('Y.m.d') }}
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">
                                            {{ $vacation->date_to->format('Y.m.d') }}
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                             <span
                                                 class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full {{ $vacation->applied ? 'bg-green-100 text-green-800' : 'bg-red-100 text-red-800' }}">
                                                    {{ $vacation->applied ? 'Да' : 'Нет' }}
                                                </span>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                        @if(auth()->user()->hasRole(\App\Models\Role::LEAD))
                                        <form action="{{ route('vacations.apply', $vacation) }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="apply"
                                                   value="{{ $vacation->applied ? 0 : 1 }}">
                                            @if(!$vacation->applied)
                                            <button
                                                type="submit"
                                                class="inline-flex justify-center py-1 px-2 border border-transparent shadow-sm text-sm font-semibold rounded-md text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:none"
                                            >
                                                Подтвердить
                                            </button>
                                            @endif
                                            @if($vacation->applied)
                                            <button
                                                type="submit"
                                                class="inline-flex justify-center py-1 px-2 border border-transparent shadow-sm text-sm font-semibold rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:none"
                                            >
                                                Отклонить
                                            </button>
                                            @endif
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5" class="text-center py-3">
                                       Запланированные даты отсутствуют
                                    </td>
                                </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            {{ $vacations->links() }}
        </div>
    </div>
</x-app-layout>
