<?php

namespace App\Http\Controllers;

use App\Http\Requests\Vacation\AppliedRequest;
use App\Http\Requests\Vacation\SearchRequest;
use App\Http\Requests\Vacation\SaveRequest;
use App\Models\Vacation;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;

class VacationController extends Controller
{
    private const PER_PAGE = 20;

    /*
     * Return user vacations list
     */
    public function index()
    {
        $vacations = Vacation::with('user')
            ->orderBy('id', 'desc')
            ->paginate(self::PER_PAGE);

        return view('vacation.index', compact('vacations'));
    }
    /*
     * Create user vacation
     * SaveRequest - middleware validation data type
     */
    public function save(SaveRequest $saveRequest): RedirectResponse
    {
        if (auth()->user()->vacations()->firstWhere('applied', true)) {
            return redirect()
                ->back()
                ->with('error_store', 'У вас уже есть запланированная дата');
        }

        auth()->user()->vacations()->create([
            'date_from' => $saveRequest->input('date_from'),
            'date_to' => $saveRequest->input('date_to')
        ]);

        return redirect()
            ->route('vacations.index')
            ->with('success_created', 'Отпуск успешно создан');
    }

    /*
     * Apply user vacation
     * AppliedRequest - middleware validation data type
     */
    public function apply(Vacation $vacation, AppliedRequest $appliedRequest): RedirectResponse
    {
        $vacation->applied = $appliedRequest->input('apply');
        $vacation->save();
        return redirect()->back();
    }

    /*
     * Search user vacations
     * SearchRequest - middleware validation data type
     * Сhecking vacation dates
     * Сheck of vacation confirmation
     */
    public function search(SearchRequest $request)
    {
        $dateFrom = Carbon::parse($request->input('date_from'));
        $dateTo = Carbon::parse($request->input('date_to'));

        if (!!$dateFrom->diff($dateTo)->invert) {
            session()->flash('error_search', 'Дата окончания не может быть меньше даты начала');
            return redirect()->route('vacations.index');
        }

        if (auth()->user()->vacations()->firstWhere('applied', true)) {
            session()->flash('error_search', 'У вас уже есть запланированная дата');
            return redirect()->route('vacations.index');
        }

        $isApplied = !!auth()->user()->vacations()->firstWhere('applied', true);
        $vacations = Vacation::searchByDate($dateFrom, $dateTo)
            ->with('user')
            ->paginate(self::PER_PAGE);

        return view('vacation.index', compact('vacations', 'isApplied'));
    }
}
