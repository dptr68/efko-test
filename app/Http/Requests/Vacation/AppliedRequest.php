<?php

namespace App\Http\Requests\Vacation;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class AppliedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasRole(Role::LEAD);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'apply' => ['required', 'boolean']
        ];
    }
}
