<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Vacation extends Model
{
    use HasFactory;

    protected $fillable = [
        'date_from',
        'date_to'
    ];

    protected $casts = [
        'date_from' => 'date',
        'date_to' => 'date',
        'applied' => 'boolean'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /*
     * Search user vacations by date return query
     * Add scope in function SearchByDate tщ added query params in function
     */
    public static function scopeSearchByDate(Builder $query, Carbon $dateFrom, Carbon $dateTo): Builder
    {
        return $query
            ->where(function (Builder $query) use ($dateFrom, $dateTo) {
                return $query
                    ->whereDate('date_from', '<=', $dateFrom)
                    ->whereDate('date_to', '>=', $dateFrom);
            })->orWhere(function (Builder $query) use ($dateFrom, $dateTo) {
                return $query
                    ->whereDate('date_from', '>=', $dateFrom)
                    ->whereDate('date_to', '<=', $dateTo);
            })->orWhere(function (Builder $query) use ($dateFrom, $dateTo) {
                return $query
                    ->whereDate('date_from', '>=', $dateFrom)
                    ->whereDate('date_to', '<=', $dateTo);
            });
    }
}
